all: talk.tex
	lualatex talk.tex
	biber talk.bcf
	lualatex talk.tex

fast: talk.tex
	lualatex talk.tex
clean:
	rm *.aux *.bbl *.bcf *.blg *.log *.nav *.out *.snm *.toc *.xml


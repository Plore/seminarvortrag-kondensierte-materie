\documentclass[10pt]{beamer}
\input{header_physics.tex}

%\AtBeginSection[]
%{
%  \begin{frame}
%    \frametitle{Übersicht}
%    \tableofcontents[currentsection]
%  \end{frame}
%}

\title{Der asymmetrische Exklusionsprozess (ASEP)}
\subtitle{Seminarvortrag\\ Theoretische Probleme der kondensierten Materie}
\author{Peter Lorenz}
\date{04.02.2015}

\begin{document}

{
\setbeamertemplate{headline}{}
\setbeamertemplate{footline}{}
\begin{frame}
  \titlepage
\end{frame}
}

\begin{frame}{Übersicht}
  \tableofcontents
\end{frame}

\section{Einleitung}
\begin{frame}{Modell}
  \begin{itemize}
    \item Teilchen mit Hardcore-Abstoßung auf eindimensionalem Gitter
    \item Wenn erlaubt: Sprung nach rechts mit Wahrscheinlichkeit $p$, nach links mit Wahrscheinlichkeit $q < p$
    \item Befüllung am linken Rand mit $\alpha$, Entnahme am rechten Rand mit $\beta$%; $\alpha, \beta < p$
    \item Häufig $q = 0$ (TASEP), $p = 1$ \mbox{\rightarrow} Festlegung der Zeitskala
  \end{itemize}
  \begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/asep}
  \end{figure}
  \begin{eqn}
    s_i = 0,1 \quad \rightarrow \quad \avg{s_i} = \sum_{\{s_j\}} s_i P_N(\{s_j\})
  \end{eqn}
\end{frame}

\begin{frame}{Zeitentwicklung (p=1)}
  \begin{block}{Ziel}
    Bestimmung von
    \begin{eqn}
      \rho_i = \avg{s_i} \qquad J_i = \avg{s_i(1-s_{i+1})}
    \end{eqn}
    im stationären Zustand $\dd{\avg{s_i}}{t} \equiv 0$ \mbox{\rightarrow} Erwartung: $J_i \equiv J$
  \end{block}
  Dynamik: 
  \begin{eqns}
  \frac{\d \avg{s_i}}{\d t} &=& \avg{s_{i-1}} - \avg{s_i} - \avg{s_{i-1}s_i} + \avg{s_is_{i+1}} \\
                            &=& \avg{s_{i-1}(1-s_i)} - \avg{s_i(1-s_{i+1})} \:, \quad i \neq 1,N \\
  \dd{\avg{s_1}}{t} &=& \alpha \avg{(1 - s_1)} - \avg{s_1(1 - s_2)} \\
  \dd{\avg{s_N}}{t} &=& \avg{s_{N-1}(1 - s_N)} - \beta \avg{s_N}
  \end{eqns}
\end{frame}

\begin{frame}{Updateschemata}
  %Zeitintervall $\d t$ hängt von Updateschema ab:
  \begin{itemize}
    \item Kontinuierliche Zeit:
    \begin{itemize}
      \item random sequential (exponentielle Wartezeit) %$\Rightarrow \d t = \frac{1}{N+1}$
    \end{itemize}
    \item Diskrete Zeit:
    \begin{itemize}
      \item sublattice parallel
      \item ordered sequential (vorwärts oder rückwärts)
      \item parallel \mbox{\rightarrow} Automatenregeln, Verkehrssimulation
    \end{itemize}
  \end{itemize}
  \mbox{\rightarrow} Updates in diskreter Zeit effizienter für MC-Simulation \par
  Äquivalenz zwischen sublattice parallel und ordered sequential \cite{evans_rajewsky_speer_1999}:
  \begin{eqns}
    J_N^{\leftarrow} &=& J_N^{\rightarrow} = J_N^{\t{sp}} = J \qquad \rho_{i,N}^{\rightarrow} = \rho_{i,N}^{\leftarrow} - J \\
    \rho_{i,N}^{\t{sp}} &=& \begin{cases}
      \rho_{i,N}^{\leftarrow}\:, & i \t{ gerade} \\
      \rho_{i,N}^{\rightarrow}\:, & i \t{ ungerade}
    \end{cases}
    \quad
    \rho_{i,N}^{\t{sp}*} = \begin{cases}
      \rho_{i,N}^{\leftarrow}\:, & i \t{ ungerade} \\
      \rho_{i,N}^{\rightarrow}\:, & i \t{ gerade}
    \end{cases}
  \end{eqns}
\end{frame}

\section{Lösungsmethoden}
\begin{frame}{Korrelationsfunktionen}
  \fontsize{9}{9}
  Dynamik für benachbarte Sites:
  \begin{eqn}
    \dd{\avg{s_i s_{i+1}}}{t} = \avg{s_{i-1}s_{i+1}} - \avg{s_i s_{i+1}} - \avg{s_{i-1}s_i s_{i+1}} + \avg{s_i s_{i+1} s_{i+2}}
  \end{eqn}
  Problem: Zur Berechnung zeitabhängiger Korrelationsfunktionen von Ordnung $m$ werden weitere Korrelationsfunktionen von Ordnung $m$ und $m+1$ benötigt
  \vskip 0.5cm
  Dilemma äquivalent ausdrückbar durch Wahrscheinlichkeiten $P_N(\{s_i\})$:
  \vskip 0.5cm
  Stationärer Zustand $\dd{P_N(\{s_i\})}{t} = 0$ \mbox{\rightarrow} Lösen von $2^N$ gekoppelten Gleichungen \cite{derrida_domany_mukamel_1992}
\end{frame}

\begin{frame}{Gewichte \cite{derrida_domany_mukamel_1992}, \cite{derrida_evans_1993}}
  \begin{eqn}
    P_N(\{s_i\}) = \frac{f_N(\{s_i\})}{\sum_{\{s_j\}} f_N(\{s_j\})}
  \end{eqn}
  Korrelationen
  \begin{eqn}
    \avg{s_{i_1} s_{i_2} \ldots s_{i_n}} = \frac{\sum_{\{s_j\}} s_{i_1} s_{i_2} \ldots s_{i_n} f_N(\{s_j\})}{\sum_{\{s_j\}} f_N(\{s_j\})}
  \end{eqn}
  Rekursion in der Systemgröße
  \begin{eqns}
    f_N &=& \begin{cases}
      \alpha f_{N-1} & \t{für } s_N=1 \\
      \alpha \beta \left[f_{N-1}(s_p = 1) + f_{N-1}(s_p=0)\right] & \t{für } s_p = 1, s_{p+1} = \cdots=s_{N} = 0 \\
      \beta f_{N-1} & \t{für } s_i \equiv 0
    \end{cases} \\
    f_1(1) &=& \alpha\:, \quad f_1(0) = \beta
  \end{eqns}
  \mbox{\rightarrow} Lösbar für beliebige $\alpha$, $\beta$, aber exponentieller Aufwand
\end{frame}

%\begin{frame}{Explizite Lösungen}
%  \begin{eqns}
%    \avg{s_k}             &=& \frac{1}{A(N+1)} \sum_{p=0}^{N-k} A(p) A(N-k) \\
%    \avg{s_{k_1} s_{k_2}} &=& \frac{1}{A(N+1)} \sum_{p_1=0}^{N-k_1} A(p_1) \sum_{p_2=0}^{N-1-k_1-p_1} A(p_2) A(N - 1 - p_1 - p_2) \\
%    A(m)                  &=& \frac{(2m)!}{m!(m+1)!}
%  \end{eqns}
%  \vskip 0.5cm
%  Probleme
%  \begin{itemize}
%    \item Immer noch mühsam für sehr große $N$
%    \item Noch umständlicher für höhere Korrelationsfunktionen
%  \end{itemize}
%\end{frame}

\begin{frame}{Meanfield-Ansatz}
  Für $\alpha + \beta = 1$ gilt \cite{derrida_domany_mukamel_1992}
  \begin{eqns}
    f_N(\{s_i\}) &=& \alpha^T \beta^{N-T}\:, \quad T = \sum_{i=1}^N s_i\\
    P_N(\{s_i\}) &=& \prod_{i=1}^N \alpha s_i + \beta (1-s_i)
  \end{eqns}
  und alle Korrelationsfunktionen faktorisieren \par
  \mbox{\rightarrow} Stationärer Zustand $\left(\dd{\avg{s_i}}{t} \equiv 0\right)$:
  \begin{eqns}[rClCrCl]
    \avg{s_i} - \avg{s_{i+1} s_i} &=& \avg{s_{i-1}} - \avg{s_i s_{i-1}} &\Rightarrow& \rho_i (1 - \rho_{i+1}) &=& \rho_{i-1}(1 - \rho_i)\\
    \avg{s_1} - \avg{s_1 s_2}     &=& \alpha(1-\avg{s_1})               &\Rightarrow& \rho_1 (1 - \rho_2)     &=& \alpha (1- \rho_1) \\
    \beta \avg{s_N}               &=& \avg{s_{N-1}} - \avg{s_N s_{N-1}} &\Rightarrow& \beta \rho_N         &=& \rho_{N-1}(1 - \rho_N) 
    %\avg{s_1}                     &=& \alpha                            &           &                   & &
  \end{eqns}
\end{frame}

\begin{frame}{Meanfield-Ansatz: Ergebnis}
  \begin{block}{Rekursion}
    \begin{eqn}
      \rho_{i+1} = 1 - \frac{J}{\rho_i} \quad \Rightarrow \quad \rho_{\pm} = \frac{1}{2}\left(1 \pm \sqrt{1-4J}\right)\:, \quad J < \frac{1}{4}
    \end{eqn}
  \end{block}
  \begin{eqn}
    \rho_i = \frac{-\rho_+ \rho_- (\rho_+^{i-1} - \rho_-^{i-1}) + (\rho_+^i - \rho_-^i) \rho_1}{-\rho_+ \rho_-(\rho_+^{i-2} - \rho_-^{i-2}) +(\rho_+^{i-1} - \rho_-^{i-1}) \rho_1}\:, \quad \rho_N = f(J, \rho_1)
  \end{eqn}
  \begin{itemize}
    \item $\rho_1$, $\rho_N$ und $J$ durch $\alpha$ und $\beta$ bestimmt
    \item Drei Phasen im Bulk:
      \begin{eqns}[CCl]
        \t{1) } & \alpha > \frac{1}{2}\:,   & \beta > \frac{1}{2}\:, \t{maximaler Strom} \\
        \t{2) } & \alpha \le \frac{1}{2}\:, & \beta > \alpha\:, \t{niedrige Dichte} \\
        \t{3) } & \beta \le \frac{1}{2}\:,  & \alpha > \beta\:, \t{hohe Dichte}
      \end{eqns}
  \end{itemize}
\end{frame}

\begin{frame}{Phasendiagramm}
  \begin{columns}
    \column{0.45\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{figures/phase_diagram_new}
      \caption{Phasendiagramm des ASEP nach der Meanfieldmethode.}
      \label{fig:phase_diagram}
    \end{figure}
    \column{0.5\textwidth}
    \begin{eqns}[CrClrCl]
      \t{1) } & J &=& \frac{1}{4}      & \quad \rho &=& \frac{1}{2} \\
      \t{2) } & J &=& \alpha(1-\alpha) & \quad \rho &=& \alpha \\
      \t{3) } & J &=& \beta(1-\beta)   & \quad \rho &=& 1-\beta
    \end{eqns}
    \begin{itemize}
      \item Phasenübergang {\color{red}erster Ordnung} zwischen 2) und 3)
      \item Phasenübergang {\color{darkgreen}zweiter Ordnung} zwischen 1) und 2) sowie 1) und 3)
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Wellen}
  \fontsize{9}{9}
  \begin{columns}
    \column{0.5\textwidth}
    \begin{eqns}[C]
      J(\rho)   =  \rho (1 - \rho) \\
      J_\t{max} = J(\rho^*)\:, \quad J_\pm     =  J(\rho_\pm)
    \end{eqns}
    Betrachte
    \begin{eqns}[C]
      v_c =  \pd{J}{\rho}\:, \quad v_s =  \frac{J_+ - J_-}{\rho_+ - \rho_-}
    \end{eqns}
    \column{0.5\textwidth}
    \begin{figure}[t]
      \centering
      \includegraphics[width=\textwidth]{figures/shock_velocity}
      \captionsetup{skip=0cm}
    \end{figure}
    \vspace{-0.5cm}
    \hspace{160pt}\cite{antal_schütz_2000}
  \end{columns}
  \begin{columns}
    \column{0.51\textwidth}
    \framebox{$\rho_+ < \rho^*$}
    \begin{itemize}
      \item $\rho_- < \rho^* \quad \rightarrow \quad v_c > 0, \rho = \rho_-$
      \item $\rho_- = \rho^* \quad \rightarrow \quad v_c = 0, \rho = \rho^*$
      \item $\rho_- > \rho^* \quad \rightarrow \quad v_c < 0, \rho = \rho^*$
      \item[$\Rightarrow$] Kontinuierlicher Phasenübergang $2) \leftrightarrow 1)$ und analog $1) \leftrightarrow 3)$
    \end{itemize}
    \column{0.51\textwidth}
    \framebox{$\rho_+ > \rho^*$}
    \begin{itemize}
      \item $J_+ > J_- \quad \rightarrow \quad v_s > 0, \rho \approx \rho_-$
      \item $J_+ = J_- \quad \rightarrow \quad v_s = 0, \rho$ steigt linear
      \item $J_+ < J_- \quad \rightarrow \quad v_s < 0, \rho \approx \rho_+$
      \item[$\Rightarrow$] Diskreter Phasenübergang $2) \leftrightarrow 3)$
    \end{itemize}
  \end{columns}
 % \par
 % \begin{eqn}
 %   J = \begin{cases}
 %     \max_{\rho \in [\rho_R, \rho_L]} J(\rho) & \t{für } \rho_L > \rho_R \\
 %     \min_{\rho \in [\rho_L, \rho_R]} J(\rho) & \t{für } \rho_R > \rho_L \\
 %   \end{cases}
 % \end{eqn}
\end{frame}

\begin{frame}{Exakte Lösung}
  \begin{itemize}
    \item ASEP außerhalb von $\alpha + \beta = 1$ exakt lösbar
    \item Bethe-Ansatz \cite{derrida_1998} \mbox{\rightarrow} kompliziert
    \item Matrixprodukt-Ansatz \cite{derrida_evans_hakim_pasquier_1993} \mbox{\rightarrow} intuitiver, mit Anpassungen durchführbar für alle Updateschemata \cite{rajewsky_santen_schadschneider_schreckenberg_1998}
  \end{itemize}
  $\Rightarrow$ Definiere Gewichte als
  \begin{eqns}
    f_N(\{s_i\})         &=&  \Braket{W | \prod_{i=1}^N (1-s_i) E + s_i D | V}\\
    \t{Bsp.: } f_6(100010) &=& \Braket{W | DEEEDE | V}\\
                   D + E &=&  DE\;\quad D \Ket{V} = \frac{1}{\beta}\Ket{V}\;\quad \Bra{W}E = \frac{1}{\alpha}\Bra{W}
  \end{eqns}
\end{frame}

\begin{frame}{Matrixprodukt-Ansatz}
  Definiere $C := D + E$
  \begin{eqns}
    \avg{s_i}     &=& \frac{\Braket{W | C^{i-1} D C^{N-i} | V}}{\Braket{W | C^N | V}}\\
    \avg{s_i s_j} &=& \frac{\Braket{W | C^{i-1} D C^{j-i-1} D C^{N-j} | V}}{\Braket{W | C^N | V}}\\
  \end{eqns}
  Nach Algebra
  \begin{eqns}[C]
    \alpha \Braket{W | E C^{N-1} | V} = \Braket{W | C^{i-1}DE C^{N-i-1} | V} = \Braket{W | C^{N-1} D | V} \beta \\
    = \Braket{W | C^{N-1} | V}
  \end{eqns}
  \mbox{\rightarrow} Strom unabhängig von Site $i$ für $\dd{\avg{s_i}}{t} \equiv 0$
\end{frame}

\begin{frame}{Form der Matrizen und Vektoren}
  \fontsize{9}{9}
  \underline{Fall $1$}: $D$ und $E$ kommutieren
  \begin{eqns}[l]
    \frac{1}{\alpha \beta} \Braket{W | V} = \Braket{W|ED|V} = \Braket{W|DE|V} = \Braket{W|D + E|V} = \left(\frac{1}{\alpha} + \frac{1}{\beta}\right) \Braket{W|V}\\
    \Rightarrow \alpha + \beta = 1
  \end{eqns}
  \underline{Fall $2$}: $D$ und $E$ kommutieren nicht \mbox{\rightarrow} unendlich-dimensionale Matrizen, mehrere Möglichkeiten \cite{derrida_evans_hakim_pasquier_1993}
  \begin{eqns}
    D = \begin{pmatrix}
      \frac{1}{\beta} & a & 0 & 0 & \ldots \\
      0               & 1 & 1 & 0 &        \\
      0               & 0 & 1 & 1 &        \\
      0               & 0 & 0 & 1 & \ddots \\
      \vdots          &   &   &   & \ddots
    \end{pmatrix}
    \quad
    E = \begin{pmatrix}
      \frac{1}{\alpha} & 0 & 0 & 0 & \ldots \\
      a                & 1 & 0 & 0 &        \\
      0                & 1 & 1 & 0 &        \\
      0                & 0 & 1 & 1 &        \\
      \vdots           &   &   & \ddots & \ddots
    \end{pmatrix} \\
    \Bra{W} = (1,0,0,\ldots) \qquad \Ket{V} = (1,0,0,\ldots)^T \qquad a = \frac{\alpha + \beta - 1}{\alpha \beta}
  \end{eqns}
\end{frame}

\begin{frame}{Lösung für $N \rightarrow \infty$}
  \begin{columns}
    \column{0.5\textwidth}
    \begin{eqn}
      J = \frac{\Braket{W|C^{N-1}|V}}{\Braket{W|C^N|V}}
    \end{eqn}
    \column{0.5\textwidth}
    Kompliziertere Formel für $\avg{s_i}$, abhängig von $i$ \par
    Asymptotik $N\rightarrow\infty$ im Bulk:
  \end{columns}
  \vspace{0.1cm}
  \begin{columns}
    \column{0.5\textwidth}
    \begin{eqn}
      J = \begin{cases}
        \frac{1}{4} & \t{für } \alpha, \beta \ge \frac{1}{2} \\
        \alpha(1-\alpha) & \t{für } \alpha<\frac{1}{2}, \beta > \alpha \\
        \beta(1-\beta) & \t{für } \beta<\frac{1}{2}, \alpha > \beta
      \end{cases}
    \end{eqn}
    \column{0.5\textwidth}
    \begin{eqn}
      \avg{s_i} = \begin{cases}
        \frac{1}{2} & \t{für } \alpha, \beta \ge \frac{1}{2} \\
        \alpha & \t{für } \alpha<\frac{1}{2}, \beta > \alpha \\
        1-\beta & \t{für } \beta<\frac{1}{2}, \alpha > \beta
      \end{cases}
    \end{eqn}
  \end{columns}
  \vspace{0.5cm}
  \begin{columns}
    \column{0.6\textwidth}
    \mbox{\rightarrow} Übereinstimmung mit Meanfield-Ansatz
    \column{0.4\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/phase_diagram_new}
  \end{columns}
\end{frame}


\section{Anwendungen}

\begin{frame}{ASEP als Wachstumsprozess}
  \begin{eqn}
    \left.
    \begin{IEEEeqnarraybox}[\IEEEeqnarraystrutmode \IEEEeqnarraystrutsizeadd{2pt}{2pt}][c]{rClCrCl}
      s_i &=& 0 & \quad \Rightarrow \quad & h_i &=& h_{i-1} + 1\\
      s_i &=& 1 & \quad \Rightarrow \quad & h_i &=& h_{i-1} - 1
    \end{IEEEeqnarraybox}
    \right\}
    \quad h(k) = \sum_{i=1}^k 1 - 2s_i
  \end{eqn}
  Deposition an lokalen Minima $h_i < h_{i+1}, h_{i-1}$\\
  $\alpha, \beta$: Inhomogenitäten
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/growth}
  \end{figure}
\end{frame}

\begin{frame}{Klassifikation}
  KPZ-Gleichung (Kardar, Parisi, Zhang)
  \begin{eqn}
    \pd{h}{t} = \nu \frac{\partial^2 h}{\partial x^2} + \frac{\lambda}{2} \left(\pd{h}{x}\right)^2 + \xi
  \end{eqn}
  \vspace{-0.5cm}
  \begin{itemize}
    \item Allgemeine Gleichung für viele stochastische Wachstumsmodelle
    \item Zugehörige KPZ-Universalitätsklasse definiert durch Skalenverhalten der Rauigkeit
  \end{itemize}
  \begin{eqns}[C]
    \sigma = \sqrt{\frac{1}{L} \int_0^L \left(h(x) - \overline{h}\right)^2 \d x}\\
    \sigma \approx L^\alpha f\left(\frac{\mean{h}}{L^\gamma}\right)\:, \qquad f \xrightarrow{L \ll \mean{h}} \t{const}, \qquad f \xrightarrow{\mean{h}\ll L} A \cdot L^{\frac{\alpha}{\gamma}} \\
    \t{mit } \alpha = \frac{1}{2} \qquad \gamma = \frac{3}{2}
  \end{eqns}
  \mbox{\rightarrow} ASEP-Wachstum liegt in KPZ-Universalitätsklasse
\end{frame}

\begin{frame}{ASEP für Verkehrssimulation}
  \begin{columns}
    \column{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/jamcartoon}
    \end{columns}
  \begin{itemize}
    \item Als Grundlage mehrerer Verkehrsflussmodelle in $1$D
    \item Betrachte Fluss $J(\rho)$ als Funktion der Verkehrsdichte im Bulk
    \item $\alpha, \beta$ beschreiben Ein-/Ausmünden des Verkehrs an Verengungen auf eine Bahn, Kreuzungen o.Ä.
    \item Phasenübergang erster Ordnung zwischen 2) und 3) experimentell beobachtbar
   % \item Phasenübergänge zweiter Ordnung z.B. in Nagel-Schreckenberg-Modell
  \end{itemize}
\end{frame}

\begin{frame}{Anpassungen \cite{antal_schütz_2000}}
  \fontsize{9}{9}
  \begin{columns}
    \column{0.60\textwidth}
    \begin{itemize}
      \item uniforme Bewegungsrate $p$
        \begin{eqn}
          J(\rho) = \rho(1-\rho)
        \end{eqn}
        symmetrisch um $\rho^* = \frac{1}{2}$ \mbox{\rightarrow} unrealistisch
      \item Fahrer passen Geschwindigkeit an umgebenden Verkehr an
      \item Führe zwei Hoppingraten ein
      \begin{itemize}
        \item $100 \rightarrow 010$ mit $r$
        \item $101 \rightarrow 011$ mit $q<r$
      \end{itemize}
    \end{itemize}
    \begin{eqns}
      J_i = \avg{s_i(1-s_{i+1})(qs_{i+2} + r(1-s_{i+2}))} \\
      \Rightarrow J(\rho) = r \rho \left(1 + \frac{\sqrt{1 - 4\rho (1- \rho)(1-\frac{q}{r})} - 1}{2 (1-\rho)(1-\frac{q}{r})}\right)
    \end{eqns}
    \column{0.40\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{current_density}
      \label{fig:current_density}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{Phasendiagramm für angepassten ASEP}
  \begin{columns}
    \column{0.5\textwidth}
    Meanfield-Lösung:
    \begin{eqns}[C]
      \rho_i = \frac{J}{(1-\rho_{i+1})(q\rho_{i+2} + r(1-\rho_{i+2}))}\\
      \rho_R = \rho_N\:, J \t{ vorgegeben}
    \end{eqns}
    \mbox{\rightarrow} \t{iteratives Lösen der Rekursion}\\
    \column{0.5\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{figures/phase_diagram_modified}
      \caption{Phasendiagramm nach Meanfield-Ansatz (gestrichelt) und exakter Lösung \cite{antal_schütz_2000}}
      \label{fig:asep_traffic}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{Bewegung von Motorproteinen \cite{lipowsky_klumpp_nieuwenhuizen_2001}}
  \begin{columns}
    \column{0.35\textwidth}
    \centering
    \includegraphics[height=0.9\textheight]{figures/motor_kinesin}
    \cite{vale_milligan_2000}
    \column{0.6\textwidth}
    \begin{itemize}
      \item intrazellulärer Transport, Zellteilung, Reorganisation des Cytoskeletts
      \item Antrieb durch ATP-Hydrolyse
      \item Richtung durch Polarität des Mikrotubulus und der ATP-Bindung
      \item Anhaftung \mbox{\rightarrow} gerichteter Random Walk
      \item gelöst \mbox{\rightarrow} ungerichteter Random Walk
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Experimentelle Größen}
  \begin{itemize}
    \item Mittlere Anhaftzeit $\Delta t_\t{b}$, abhängig von Rauigkeit des Filaments, Ionenkonzentration etc.
    \item Anhaftgeschwindigkeit $v_\t{b}$
    \item Diffusionskonstante $D_\t{b}$ bei Anhaftung
    \item Größe von Kinesin \mbox{\rightarrow} Gitterkonstante
  \end{itemize}
    Größenordnungen:
      \begin{eqns}
        \Delta t_\t{b} & \sim & \SI{1}{\second} \\
        v_\t{b} & < & \SI{1}{$\mathup{\mu}$\metre\per\second} \\
        D_\t{b} & \sim & \SI{1e3}{\nano\metre\squared\per\second} - \SI{1e4}{\nano\metre\squared\per\second} \\
        l & = & \SI{8}{\nano\metre}
      \end{eqns}
\end{frame}

\begin{frame}{Modell}
  \vspace{0.2cm}
  \begin{columns}
    \column{0.35\textwidth}
    Bei Anhaftung:
    \begin{itemize}
      \item vorwärts mit $\sfrac{a}{\tau_\t{b}}$
      \item rückwärts mit $\sfrac{b}{\tau_\t{b}}$
      \item Desorption mit $\sfrac{\epsilon}{6 \tau_\t{b}}$ zu einer von $n_\t{ad}$ Positionen
      \item Stillstand mit \\ $c = 1-a-b-\sfrac{n_\t{ad} \epsilon}{6}$
    \end{itemize}
    \column{0.6\textwidth}
    \begin{itemize}
      \item $\tau_\t{b}$ festgelegt durch $\tau_\t{b} = \frac{l}{v_\t{b}}\left(1-\frac{2 D_\t{b}}{l v_\t{b}}\right)$ mit experimentellen Werten
      \item Vorhersagen für experimentelle Größen mit
        \begin{eqns}[C]
          \Delta t_\t{b} = \left(\frac{6}{n_\t{ad}} - \epsilon\right) \tau_\t{b} \qquad v_\t{b} = \frac{(a-b) l}{(a+b+c) \tau_\t{b}} \\
          D_\t{b}        = \left(\frac{a + b}{a+b+c} - \frac{(a - b)^2}{(a+b+c)^2}\right) \frac{l^2}{2 \tau_\t{b}}
        \end{eqns}
    \end{itemize}
    \end{columns}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/motors}
\end{frame}

\begin{frame}{Ergebnisse}
  \vspace{0.3cm}
  $1$D-Filamente: Experimentelle Größen in verschiedenen Geometrien reproduzierbar mit
  \begin{eqns}[C]
    b = 0 \qquad \tau_\t{b} = \SI{5.9}{\second}\\
    a = 0.4975 \qquad c = 0.4987 \qquad \epsilon = 0.0075
  \end{eqns}
  \vspace{-0.5cm}
  \begin{columns}
    \column{0.5\textwidth}
    %\begin{itemize}
    Bei geschlossener Geometrie: selbstorganisierter Dichtegradient \vskip 0.2cm
        \mbox{\rightarrow} rein diffusiver Rücktransport von Motoren
      %\item Überlagerung von ASEP-Dynamik mit Langmuir-Kinetik
    %\end{itemize}
    \column{0.5\textwidth}
    \begin{figure}[t]
      \centering
      \includegraphics[width=\textwidth]{figures/motor_gradient}
      \label{fig:motor_gradient}
      \vspace{-0.9cm}
    \end{figure}\cite{lipowsky_klumpp_nieuwenhuizen_2001}
  \end{columns}
\end{frame}

\section{Zusammenfassung}

\begin{frame}{Zusammenfassung}
  \begin{itemize}
    \item ASEP ist ein einfaches Modell, an dem sich viele Phänomene studieren lassen
    \item Meanfield-Theorie liefert drei Phasen in Abhängigkeit von $\alpha$ und $\beta$\\
      \mbox{\rightarrow} exakte Lösung bestätigt dies
    \item Phasenübergänge lassen sich qualitativ durch Schockwellen und CMS-Bewegung verstehen
    \item Anwendungen als Wachstumsmodell, als Grundlage für Verkehrsmodelle sowie Simulation von Motorproteinbewegung
  \end{itemize}
\end{frame}

{
\setbeamertemplate{headline}{}
\setbeamertemplate{footline}{}
\begin{frame}
  \centerline{Vielen Dank für die Aufmerksamkeit!}
\end{frame}
}

\begin{frame}[allowframebreaks]{Quellen}
  \printbibliography\relax
\end{frame}

\begin{frame}{Dynamik-Details}
  \begin{eqn}
   s_i(t+\d t) = \begin{cases}
   s_i(t) & \t{mit } 1 - 2 \d t \\
   s_{i-1}(t) + s_i(t) - s_{i-1}(t)s_i(t) & \t{mit } \d t \\
   s_i(t)s_{i+1}(t) & \t{mit } \d t
 \end{cases} 
 \end{eqn}
 \begin{eqn}
    s_i(t+\d t)s_{i+1}(t+\d t) = \begin{cases}
      s_i(t) s_{i+1}(t)                                              & \t{mit } 1 - 3 \d t \\
      \left(s_{i-1}(t) + s_i(t) - s_{i-1}(t)s_i(t)\right) s_{i+1}(t) & \t{mit } \d t \\
      s_i(t) s_{i+1}(t)                                              & \t{mit } \d t \\
      s_i(t) s_{i+1}(t) s_{i+2}(t)                                   & \t{mit } \d t
    \end{cases}
  \end{eqn}
\end{frame}

\begin{frame}{Vergleich der Phasen für verschiedene Updateschemata \cite{rajewsky_santen_schadschneider_schreckenberg_1998}}
  \fontsize{9}{9}
  \vspace{-0.7cm}
  \begin{eqns}[Clll]
    & \t{random sequential} &      \t{ordered sequential} &                                \t{parallel}
    \vspace{.2cm} \\
  1) \qquad &  \rho = \frac{1}{2}         \qquad & \rho = \frac{1}{1+\sqrt{1-p}}                     \qquad & \rho = \frac{1}{2} \\
           &  J    = \frac{p}{4}         \qquad & J    = \frac{1-\sqrt{1-p}}{1+\sqrt{1+p}}          \qquad & J    = \frac{1-\sqrt{1-p}}{2}
    \vspace{.3cm} \\
  2) \qquad& \rho = \alpha              \qquad & \rho = \frac{\alpha}{p}                           \qquad & \rho = \frac{\alpha(1-\alpha)}{p-\alpha^2} \\
           & J    = p \alpha (1-\alpha) \qquad & J    = \frac{\alpha}{p} \frac{p-\alpha}{1-\alpha} \qquad & J    = \alpha \frac{p-\alpha}{p-\alpha^2}
    \vspace{.4cm} \\
  3) \qquad&\rho = 1 - \beta           \qquad & \rho = \frac{1}{p}\frac{p-\beta}{1-\beta}         \qquad & \rho = \frac{p-\beta}{p-\beta^2} \\
            &J    = p \beta (1-\beta)   \qquad & J    = \frac{\beta}{p}\frac{p-\beta}{1-\beta}     \qquad & J    = \beta \frac{p-\beta}{p-\beta^2}
  \end{eqns}
\end{frame}

\begin{frame}{Burgers-Gleichung}
  Unviskoser Grenzfall
  \begin{eqn}
    \pd{u}{t} + u \pd{u}{x} = 0\:, \qquad u(x,t) = -\lambda \pd{h}{x}
  \end{eqn}
  \begin{columns}
    \column{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/growth_burgers} 
    \cite{kardar_parisi_zhang_1986}
    \column{0.5\textwidth}
    \begin{itemize}
      \item Modell für eindimensionale Strömung
      \item ASEP approximiert Burgers-Gleichung
      \item Bei geeigneten Anfangswerten Schockwellen \\
        \mbox{\rightarrow} parabolisches Höhenprofil
    \end{itemize}
  \end{columns}
\end{frame}

\end{document}

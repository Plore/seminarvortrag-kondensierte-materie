import matplotlib.pyplot as plt
import numpy as np

#plt.rc("text", usetex=True)
#plt.rc("font", family="serif")
#plt.rc("font", size=20)

#r = 1
#q = 0.1
#
#fig = plt.figure(figsize=(6,7))
#ax = fig.add_subplot(111)
#rhos = np.linspace(0,1,1000)
#js   = rhos * (1 - rhos)
#jsp  = r * rhos * (1 + (np.sqrt(1 - 4 * rhos * (1 - rhos) * (1 - q / r)) - 1) / (2 * (1 - rhos) * (1 - q / r)))
#ax.plot(rhos, js, 'r-', label="$p = 1$")
#ax.plot(rhos, jsp, 'b-', label="$r = 1, q = 0.1$")
#ax.set_xlabel(r"$\rho$")
#ax.set_ylabel(r"$J$")
#ax.legend(loc="best")
#fig.tight_layout()
#fig.savefig("current_density.pdf")

ts = np.linspace(0, 0.5, 100)
plt.plot(ts, ts, 'r-', linewidth=2)
plt.xlim(0,1)
plt.ylim(0,1)
plt.axhline(y=0.5, xmin=0.5, xmax=1.0, color='g', linewidth=2)
plt.axvline(x=0.5, ymin=0.5, ymax=1.0, color='g', linewidth=2)
plt.xlabel(r"$\alpha$")
plt.ylabel(r"$\beta$", rotation=0, labelpad=15)
plt.text(0.7, 0.7, "$1)$")
plt.text(0.2, 0.7, "$2)$")
plt.text(0.7, 0.2, "$3)$")
plt.xticks([0, 0.5, 1.0], ["0.0", "0.5", "1.0"], ha="left")
plt.yticks([0, 0.5, 1.0], ["0.0", "0.5", "1.0"])
fig = plt.gcf()
fig.set_size_inches(5, 5)
plt.axes().set_aspect('equal')
plt.savefig("figures/phase_diagram_new.pdf")
plt.clf()

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import rand

plt.rc("text", usetex=True)

# parallel update routine
def PU(s, p, alpha, beta):
    snew = np.copy(s)
    rands = np.random.rand(N)
    if s[-1] == 1 and rands[-1] < beta:
        snew[-1] = 0
    for i in range(0, N - 1):
        if s[i] == 1 and s[i+1] == 0 and rands[i + 1] < p:
            snew[i] = 0
            snew[i + 1] = 1
    if s[0] == 0 and rands[0] < alpha:
        snew[0] = 1
    return snew

# random sequential update routine
def RSU(s, p, alpha, beta, omega_a, omega_d):
    pos = np.random.randint(0, N)
    if pos < N - 1:
        x = np.random.random()
        if s[pos] == 1 and s[pos + 1] == 0 and x < p:
            s[pos]     = 0
            s[pos + 1] = 1
    if pos == 0:
        x = np.random.random()
        if s[0] == 0 and x < alpha:
            s[0] = 1
    if pos == N - 1:
        x = np.random.random()
        if s[N - 1] == 1 and x < beta:
            s[N - 1] = 0
    # adsorption and desorption in the bulk
    if pos > 0 and pos < N - 1:
        x = np.random.random()
        if s[pos] == 0 and x < omega_a:
            s[pos] = 1
        x = np.random.random()
        if s[pos] == 1 and x < omega_d:
            s[pos] = 0

def adsorption(omega_a, s):
    occup_new = np.copy(s)
    for i in range(1, len(s) - 1):
        x = np.random.random()
        if s[i] == 0 and x < omega_a:
            occup_new[i] = 1
    return occup_new

def desorption(omega_d, s):
    occup_new = np.copy(s)
    for i in range(1, len(s) - 1):
        x = np.random.random()
        if s[i] == 1 and x < omega_a:
            occup_new[i] = 0
    return occup_new

p = 1
#alpha = 0.3
#beta = 1.0
#N = 100
#steps = 100 * N 
#xs = np.arange(0, N)
#ys = np.zeros(N)
#K = 3
#
#Omega_d = 0.000
#s = np.random.randint(low=0, high=2, size=N)
#density = np.zeros(N)
#omega_d = Omega_d / N
#omega_a = K * omega_d

#for i in range(steps):
#    s = PU(s, p, alpha, beta)
#   # print(s)
#    if i % N == 0:
#        density += s
#
#density /= (steps / N)
#plt.plot(np.linspace(0,1,N), density)
#plt.legend(loc="best")
#plt.ylim(0,1)
#plt.savefig("density_step_variation.pdf")

#Omegas_d = [0.001, 0.03, 0.05, 0.1]
#density = np.zeros(shape=(len(Omegas_d), N))
#
#for k in range(len(Omegas_d)):
#    s = np.zeros(N)
#    omega_d = Omegas_d[k] / N
#    omega_a = K * omega_d
#    for i in range(steps):
#        RSU(s, p, alpha, beta, omega_a, omega_d)
#        if i % 10 == 0:
#            density[k] += s
#
#    density[k] /= (steps / 10)
#    plt.plot(np.arange(0, N), density[k], label="$\Omega_d = {:.3f}$".format(Omegas_d[k]))
#
#plt.legend(loc="best")
#plt.ylim(0,1)
#plt.savefig("density_profile.pdf")

#density = []
#Ns = [50, 100, 300, 1000]
#for k in range(len(Ns)):
#    N = Ns[k]
#    s = np.zeros(N)
#    density.append(np.zeros(N))
#    steps = 50000 * N
#    omega_d = 0.04 / N
#    omega_a = K * omega_d
#    for i in range(steps):
#        RSU(s, p, alpha, beta, omega_a, omega_d)
#        if i % 1 == 0:
#            density[k] += s
#
#    density[k] /= (steps / 1)
#    plt.plot(np.linspace(0,1,N), density[k], label="$N = {:.1f}$".format(N))
#
#plt.legend(loc="best")
#plt.ylim(0,1)
#plt.savefig("density_profile.pdf")

N = 50
steps = 3000 * N 
K = 3
Omega_d = 0.000
s = np.random.randint(low=0, high=2, size=N)
omega_d = Omega_d / N
omega_a = K * omega_d

n_betas  = 10
n_alphas = 10
measure_pos = N / 3
flux = np.zeros(shape=(n_betas, n_alphas))
density = np.zeros(shape=(n_betas, n_alphas))
for a in np.arange(1, n_alphas + 1):
    alpha = 1 / n_alphas * a
    for b in np.arange(1, n_betas + 1):
        beta = 1 / n_betas * b
        countd = 0
        countf = 0
        count_time = 0
        time_start = False
        for t in range(steps):
            RSU(s, p, alpha, beta, omega_a, omega_d)
            if time_start:
                count_time += 1
            if s[measure_pos] == 1: 
                countd += 1
                # flux at i is <s[i](1 - s[i + 1])>
                if s[measure_pos] * (1 - s[measure_pos + 1]) == 1:
                    countf += 1
                if time_start == False:
                    time_start = True
        density[b-1][a-1] = countd / count_time
        flux[b-1][a-1] = countf / count_time

    #print(count, count_time)
    #plt.scatter(xs[occup==1], ys[occup==1], s=np.pi*10**2)
    #plt.xlim(-1, 21)
    #plt.savefig("img/asep_{:03d}".format(s))
    #plt.clf()
    
#for b in range(n_betas):
#    plt.plot(1 / n_alphas * np.arange(1, n_alphas + 1), flux[b], '.', label=r"$\beta = {}$".format(b * 1 / n_betas))

plt.imshow(flux, interpolation="nearest", extent=[0,1,0,1], origin="lower")
plt.colorbar()
plt.savefig("flux.pdf")
plt.clf()
#
#plt.imshow(density, interpolation="nearest", extent=[0,1,0,1], origin="lower")
#plt.colorbar()
#plt.savefig("density.pdf")
#plt.clf()

#for beta in [0.1, 0.3, 0.5, 0.7, 0.9]:
#    density = np.zeros(N)
#    time_start = False
#    count_time = 0
#    for s in range(steps):
#        occup, c = iterate(occup, 1, beta)
#        if c:
#            time_start = True
#        if time_start:
#            count_time += 1
#            for i in range(N):
#                density[i] += occup[i]
#    
#    density /= count_time
#    plt.plot(np.arange(len(density)), density, '.', label="alpha = {}".format(alpha))
#
#plt.ylim(0,1)
#plt.legend(loc="best")
#plt.savefig("density.pdf")
#plt.clf()
